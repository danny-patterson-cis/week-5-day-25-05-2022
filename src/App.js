import React from 'react'
import { useState } from 'react';
import './App.css';

function App() {
  const [input,setInput] = useState("")
  const [postData, setPostData] = useState([])
  

  const submit =()=>{
    let x=[];
    if (input === "") {
      alert("Please enter value")
      console.log("invalid")
    }else{
      for(let i =1;i<=parseInt(input);i++){
        x+=[i+" "];
        console.log("loop",i);
      }
      console.log(x);
      setPostData([x,...postData])

    
  }
  setInput("")
  }
  

  return (
    <div className="App">
      <div className="container-1 mb-3">
        <label className="form-label">Please Enter number</label>
        <input type="text" className="form-control" value={input} onChange={(e) => setInput(e.target.value)} />
        <div className="d-grid gap-2 d-md-block mt-2">
          <button onClick={submit} className="btn btn-primary" type="button">Submit</button>
        </div>
        
      </div>{postData.map((d,i)=>{return(<div className="" key={i}>
        
        <ul className="list-group">
          <li className="list-group-item">{d}</li>
          </ul>
        </div>)})}
      

      </div>
  );
}

export default App;
